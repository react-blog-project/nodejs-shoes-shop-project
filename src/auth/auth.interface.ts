export interface TokenResponse {
  accessToken: string;
  refreshToken: string;
}

export interface RegisterResponse {
  id: number;
  message: string;
}

export interface ForgotPasswordResponse {
  url: string;
  message: string;
}

export interface VerifyForgotPasswordResponse {
  message: string;
}
