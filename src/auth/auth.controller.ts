import { Controller, Post, UseInterceptors, Body, Query } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ExceptionInterceptor } from '../common/interceptors/exception.interceptor';
import { LoginRequest, RefreshTokenRequest, RegisterRequest } from './dto';
import {
  RegisterResponse,
  TokenResponse,
  ForgotPasswordResponse,
  VerifyForgotPasswordResponse,
} from './auth.interface';
import { ForgotPasswordRequest } from './dto/forgot-password-request.dto';
@Controller()
@UseInterceptors(ExceptionInterceptor)
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  loginManager(@Body() body: LoginRequest): Promise<TokenResponse> {
    return this.authService.login(body);
  }

  @Post('refresh-token')
  refreshTokenManager(
    @Body() body: RefreshTokenRequest,
  ): Promise<TokenResponse> {
    return this.authService.refreshToken(body.refreshToken);
  }

  @Post('register')
  registerManager(@Body() body: RegisterRequest): Promise<RegisterResponse> {
    return this.authService.register(body);
  }

  @Post('forgot-password')
  forgotPasswordManager(
    @Body() body: ForgotPasswordRequest,
  ): Promise<ForgotPasswordResponse> {
    return this.authService.forgotPassword(body);
  }

  @Post('verify-forgot-password')
  verifyForgotPasswordManager(
    @Query('token') token: string,
  ): Promise<VerifyForgotPasswordResponse> {
    return this.authService.verifyForgotPassword(token);
  }
}
