export const ACCOUNT_INACTIVE = 'INACTIVATED';
export const ACCOUNT_LOCKED = 'LOCKED';
export const ACCOUNT_ACTIVE = 'ACTIVATED';

export const ACCOUNT_TYPE_PERSONAL = 'PERSONAL';
export const ACCOUNT_TYPE_BUSINESS = 'BUSINESS';

// status order
export const STATUS_PENDING = 'PENDING';
export const STATUS_APPROVED = 'APPROVED';
export const STATUS_FAIL = 'FAILED';
export const STATUS_CANCELED = 'CANCELED';

export const STATUS_ACTIVE = 'ACTIVATED';
export const STATUS_INACTIVE = 'INACTIVATED';

export const TYPE_POST = 'POST';
export const TYPE_PRODUCT = 'PRODUCT';

export const ERROR_LOGIN_OTHER_DEVICE = 'LOGIN_OTHER_DEVICE';
export const ERROR_STATUS_USER_CHANGED = 'STATUS_USER_CHANGED';

export const ACCESS_ALL = '*';
export const ACCESS_USER = 'USER';
export const ACCESS_ROLE = 'ROLE';
export const ACCESS_STAFF = 'STAFF';
export const ACCESS_STATISTIC = 'STATISTIC';
export const ACCESS_PRODUCT = 'PRODUCT';
export const ACCESS_ARTICLE = 'ARTICLE';
export const ACCESS_SETTING = 'SETTING';

export const GENDER_MALE = 1;
export const GENDER_FEMALE = 2;
export const GENDER_OTHER = 0;
