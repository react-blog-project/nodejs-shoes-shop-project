import {
  Body,
  Controller,
  Get,
  HttpCode,
  Post,
  Query,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { AuthGuard } from 'src/common/guards/auth.guard';
import { ExceptionInterceptor } from 'src/common/interceptors/exception.interceptor';
import { Category } from './category.entity';
import { CategoryService } from './category.service';
import { CategoryGet } from './dto/category-get.dto';
import { CategoryCreate } from './dto/category-create.dto';

@Controller('categories')
@UseGuards(AuthGuard)
@UseInterceptors(ExceptionInterceptor)
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) {}

  @Post('')
  @HttpCode(201)
  createCategory(@Body() categoryCreate: CategoryCreate): Promise<Category> {
    return this.categoryService.createCategory(categoryCreate);
  }

  @Get('')
  getTreeCategory(
    @Query() categoryGet: CategoryGet,
  ): Promise<Category[] | Category> {
    if (categoryGet.idCategoryParent && categoryGet.slugCategoryParent) {
      return this.categoryService.getAllCategory();
    }
    return this.categoryService.getTreeCategory(categoryGet);
  }
}
