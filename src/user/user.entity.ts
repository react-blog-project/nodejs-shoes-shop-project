import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  Index,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import {
  ACCOUNT_INACTIVE,
  ACCOUNT_ACTIVE,
  GENDER_FEMALE,
  GENDER_MALE,
  GENDER_OTHER,
} from '../common/constant';
import { Exclude } from 'class-transformer';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @Index({ unique: true })
  email: string;

  @Column()
  @Exclude()
  password: string;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({
    type: Number,
    enum: [GENDER_MALE, GENDER_FEMALE, GENDER_OTHER],
    default: GENDER_OTHER,
  })
  gender: number;

  @Column({
    default: null,
  })
  birthday: Date;

  @Column()
  salt: string;

  @Column({ default: null })
  mobile: string;

  @Column({ default: null })
  jti: string;

  @Column({
    type: 'enum',
    enum: [ACCOUNT_INACTIVE, ACCOUNT_ACTIVE],
    default: ACCOUNT_ACTIVE,
  })
  status: string;

  @Column({ default: null })
  roleId: number;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;
}
