import { Module } from '@nestjs/common';
import { RoleService } from './role.service';
import { Role } from './role.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TokenModule } from '../common/services/token.module';
import { EncryptionModule } from '../common/services/encryption.module';
import { UserService } from '../user/user.service';
import { User } from '../user/user.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Role, User]),
    TokenModule,
    EncryptionModule,
  ],
  providers: [RoleService, UserService],
  exports: [RoleService],
})
export class RoleModule {}
